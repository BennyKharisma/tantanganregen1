<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/librarystyle.css') }}" >
</head>
<body>
    <div class="navbar">        
         <div class="navs">
            <h1>Book Library</h1>
            <a href="/">Add New Book</a>
        </div>
    </div>

    <br><br><br>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Book Name</th>
                <th scope="col">Book Genre</th>
                <th scope="col">Stock</th>
                <th scope="col">Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($temp as $book)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$book->book_name}}</td>
                <td>{{$book->book_genre}}</td>
                <td>{{$book->stock}}</td>
                <td>{{$book->book_price}}</td>
                <td> 
                    <div class="change">
                        <a href="{{route('book.edit', $book->id)}}">Edit</a>
                            <form action="{{route('book.delete', $book->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit">Delete</button>
                            </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>