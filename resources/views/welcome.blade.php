<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Library</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/homestyle.css') }}" >
</head>
<body>
    <div class="navbar">        
         <div class="navs">
            <h1>Book Library</h1>
            <a href="/library">View Book List</a>
        </div>
    </div>
        
    <div class="content">
        <br><br><br><br>
        <h1>Add Book</h1>
        <form class="addbook" action="/books" method="POST">
            @csrf
            <div class="bookname">
                <label for="exampleInputEmail1">Book Name</label>
                <input type="text" name="books_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="bookgenre">
                <label for="exampleInputEmail1">Book Genre</label>
                <input type="text" name="books_genre" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="stock">
                <label for="exampleInputPassword1">Book Stock</label> 
                <input type="number" name="books_stock" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="price">
                <label for="exampleInputPassword1">Book Price</label>
                <input type="number" name="books_price" class="form-control" id="exampleInputPassword1">
            </div>
            <button type="submit" class="btn btn-primary">Add New Book</button>
        </form>
    </div>
</body>
</html>