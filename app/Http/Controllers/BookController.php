<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\books;

class BookController extends Controller
{
    public function index(){
        return view('welcome');
    }

    public function addbook(Request $request){
        books::create([
            'book_name' => $request->books_name,
            'book_genre' => $request->books_genre,
            'stock' => $request->books_stock,
            'book_price' => $request->books_price
        ]);
        return redirect('/library');
    }

    public function bookList(){
        $temp = books::all();
        return view('library', compact('temp'));
    }

    public function delete($id){
        books::destroy($id);
        return back();
    }

    public function edit($id){
        $book = books::findOrFail($id);
        return view('edit', compact('book'));
    }

    public function update(Request $request, $id){
        books::findOrFail($id)->update([
            'book_name' => $request->books_name,
            'book_genre' => $request->books_genre,
            'stock' => $request->books_stock,
            'book_price' => $request->books_price
        ]);
        return redirect('/library');
    }
}
