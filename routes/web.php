<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@index');

Route::post('/books', 'BookController@addbook')->name('book.add');

Route::get('/library', 'BookController@bookList')->name('book.list');

Route::delete('/books/{id}', 'BookController@delete')->name('book.delete');

Route::get('/books/{id}', 'BookController@edit')->name('book.edit');

Route::patch('/books/{id}', 'BookController@update')->name('book.update');